#!/home/dirac/tmcs/ball4935/opt/anaconda3/bin/python3
import os
import argparse


def get_args():

    parser = argparse.ArgumentParser()
    parser.add_argument("filenames", action='store', help='.inp file(s) sumbit to the queue', nargs='+')
    parser.add_argument("-ca", action='store_true', default=False,
                        help='Copy all of the files in the current dir')
    parser.add_argument("-cs", action='store_true', default=False,
                        help="Copy all files from the scratch directory")
    parser.add_argument("-notrashtmp", action='store_true', default=False,
                        help="Don't trash the temporary files that may be generated")
    parser.add_argument("-np", type=int, default=0, help="Override the number of cores specified in the input file")

    return parser.parse_args()


def print_csh_script(inp_filename, ca, cs, notrashtmp, np):

    sh_filename = str(inp_filename.replace('.inp', '.sh'))

    keyword_line = ''
    with open(inp_filename, 'r') as inp_file:
        for line in inp_file:
            if line.startswith('!'):
                keyword_line = line

    nprocs = 1

    for item in keyword_line.split():
        if item.startswith("PAL"):
            nprocs = item[3:]

    if np != 0:
        nprocs = np

    with open(sh_filename, 'w') as csh_script:
        print('#!/bin/csh', file=csh_script)
        print('#', file=csh_script)
        print('#$ -cwd', file=csh_script)
        print('#', file=csh_script)
        print('#$ -pe smp ' + str(nprocs), file=csh_script)
        print('#$ -l s_rt=360:00:00', file=csh_script)
        print('#', file=csh_script)
        print('module load mpi/openmpi3-x86_64', file=csh_script)
        # print('setenv path \"$path\:/opt/openmpi2/bin\"', file=csh_script)
        # print('setenv ld_library_path \"/opt/openmpi2/lib\"', file=csh_script)
        print('setenv ORIG $PWD', file=csh_script)
        print('setenv SCR $TMPDIR', file=csh_script)
        print('mkdir -p $SCR', file=csh_script)
        if ca:
            print('cp * $SCR', file=csh_script)
        else:
            print('cp ' + inp_filename + ' $SCR', file=csh_script)
        print('#', file=csh_script)
        print('cd $SCR', file=csh_script)
        print(('/usr/local/orca_4_1_1_linux_x86-64/orca $SCR/' +
              inp_filename + ' > ' + inp_filename.replace('.inp', '.out')), file=csh_script)
        if notrashtmp:
            pass
        else:
            print('rm -f *.tmp', file=csh_script)
        if cs:
            print('cp * $ORIG', file=csh_script)
        else:
            print('cp *.out *.xyz $ORIG', file=csh_script)
        print('cd / ', file=csh_script)
        print('rm -Rf $SCR', file=csh_script)
        print('cd $ORIG', file=csh_script)
        print('rm *.sh.*', file=csh_script)

    return sh_filename


if __name__ == '__main__':

    args = get_args()
    for filename in args.filenames:
        script_filename = print_csh_script(filename, args.ca, args.cs, args.notrashtmp, args.np)
        os.system('qsub ' + script_filename)
