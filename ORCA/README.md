# ORCA
Using ORCA on dirac

To submit the orca_test job to the queuing system use the following command

```bash
qsub run_orca.sh
```

Alternatively, qorca.py may be used to generate a submission script
automatically. The script will submit the job using the number of cores specified in the .inp file (PAL keyword).

```bash
python qorca.py orca_test.inp
```

Also included is a DLPNO-CCSD(T) input file, using 4GB of memory for each of the 8 cores. The basis set is suitable for anions and 2nd row elements and is probably more accurate than the standard cc-pVTZ for this system.

```bash
python qorca.py dlpno_test.inp
```
