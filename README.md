# dirac
 Tutorials and scripts for Dirac

## Obtaining an account

Fill out the form at http://it.chem.ox.ac.uk/dirac.html. If there are any problems email help@itsupport.chem.ox.ac.uk.

## Logging in

To logon to Dirac on mac open a terminal window and use the following command, replacing abcd1234 with your username.

```bash
ssh -p 99 abcd1234@dirac.chem.ox.ac.uk
```

## Using the cluster

Please refer to the folders in this repo for usage of specific codes on Dirac.

To access files of the scratch directory while a job is running:

```bash
ssh comp<XXXX> #location of calculation
cd /scratch/<JOB-ID>.1.multiway.q/ #tab complete this
```
You may be asked if you wish to add the node to the list of known hosts if this is the first time you have logged in to this node. You should do this.
